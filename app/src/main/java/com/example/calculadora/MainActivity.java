package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button suma;
    private Button resta;
    private Button division;
    private Button multiplicacion;

    private EditText num1;
    private EditText num2;
    private EditText resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        num1 = findViewById(R.id.num1);
        num2 = findViewById(R.id.num2);
        resultado = findViewById(R.id.txtResultado);

        suma = findViewById(R.id.btnSuma);
        suma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                resultado.setText(op_suma(Integer.parseInt(num1.getText().toString()), Integer.parseInt(num2.getText().toString()))+ "");
            }
        });

        resta = findViewById(R.id.btnResta);
        resta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultado.setText(op_resta(Integer.parseInt(num1.getText().toString()), Integer.parseInt(num2.getText().toString()))+ "");
            }
        });

        division = findViewById(R.id.btnDivision);
        division.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultado.setText(op_division(Integer.parseInt(num1.getText().toString()), Integer.parseInt(num2.getText().toString()))+ "");
            }
        });

        multiplicacion = findViewById(R.id.btnMultiplicacion);
        multiplicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultado.setText(op_multiplicacion(Integer.parseInt(num1.getText().toString()), Integer.parseInt(num2.getText().toString()))+ "");
            }
        });


    }

    public int op_suma(int a, int b){
        return a + b;
    }

    public int op_resta(int a, int b){
        return a - b;
    }

    public int op_division(int a, int b){
        return a / b;
    }

    public int op_multiplicacion(int a, int b){
        return a * b;
    }
}